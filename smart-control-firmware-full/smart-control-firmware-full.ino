#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

/************************* WiFi Access Point *********************************/

#define WLAN_SSID       "luortegarL"
#define WLAN_PASS       "chandelle21"

/************************* Adafruit.io Setup *********************************/
#define MODULE_ID      "002"
#define AIO_SERVER      "luortegar.myftp.org"
#define AIO_SERVERPORT  2718
#define AIO_CLIENT_ID  "module#" MODULE_ID
#define AIO_USERNAME    "userModule#" MODULE_ID
#define AIO_KEY         "catOrDog#" MODULE_ID

/************ Global State (you don't need to change this!) ******************/

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_CLIENT_ID, AIO_USERNAME, AIO_KEY);

/****************************** Feeds ***************************************/

// Setup a feed called 'photocell' for publishing.
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
Adafruit_MQTT_Publish photocell = Adafruit_MQTT_Publish(&mqtt,  "sensors");

// Setup a feed called 'onoff' for subscribing to changes.
Adafruit_MQTT_Subscribe onoffbutton = Adafruit_MQTT_Subscribe(&mqtt,  "actuators");

/*************************** Sketch Code ************************************/

// Bug workaround for Arduino 1.6.6, it seems to need a function declaration
// for some reason (only affects ESP8266, likely an arduino-builder bug).
void MQTT_connect();

void setup() {
  Serial.begin(115200);
  delay(10);

 

  Serial.println(F("Adafruit MQTT demo"));

  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscription for onoff feed.
  mqtt.subscribe(&onoffbutton);
}

uint32_t x=0;

void loop() {
  // Ensure the connection to the MQTT serve.
  MQTT_connect();

  // this is our 'wait for incoming subscription packets' busy subloop
  // try to spend your time here

  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(5000))) {
    if (subscription == &onoffbutton) {
      char * buf = (char *) onoffbutton.lastread;
      Serial.print(F("Got: ")); Serial.println(buf);

      String moduleId = String(strtok_r(buf, ",",&buf));
        if(String(MODULE_ID) ==moduleId){
        char * methodName = strtok_r(buf, ",",&buf);
       
         if (String(methodName) == "digitalWrite") {
             int pin  =  String((char*)strtok_r(buf, ",",&buf)).toInt();
             boolean  state =  String(strtok_r(buf, ",",&buf)) == "true"?HIGH:LOW;
              Serial.print("#");
              Serial.print(pin);
               Serial.print("#");
              Serial.print(state);
               pinMode(pin, OUTPUT);
             digitalWrite(pin, state);
         }

        }
    }
  }

  // Now we can publish stuff!
  Serial.print(F("\nSending photocell val "));
  Serial.print(x);
  Serial.print("...");


  
  if (! photocell.publish(x++)) {
    Serial.println(F("Failed"));
  } else {
    Serial.println(F("OK!"));
  }

  // ping the server to keep the mqtt connection alive
  if(! mqtt.ping()) {
    //mqtt.disconnect();
  }
}

// Function to connect and reconnect as necessary to the MQTT server.
void MQTT_connect() {
  int8_t ret;
  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }
  Serial.print("Connecting to MQTT... ");
  uint8_t retries = 5;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // Wait 5 seconds.
       retries--;
       if (retries == 0) {
        while (1); // Basically die and wait for WDT to reset me. 
       }
  }
  Serial.println("MQTT Connected!");
}
