package com.creando.iot.smartcontrol;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
//@EnableResourceServer
public class SmartControlApp {


	public static void main(String[] args) {
		SpringApplication.run(SmartControlApp.class, args);
	}
}
