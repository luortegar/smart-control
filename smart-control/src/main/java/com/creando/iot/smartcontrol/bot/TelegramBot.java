package com.creando.iot.smartcontrol.bot;


import com.creando.iot.smartcontrol.mqtt.consumer.MqttPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;


@Component
public class TelegramBot extends TelegramLongPollingBot {

    private static final Logger logger = LoggerFactory.getLogger(TelegramBot.class);

    public static final String LAMP_LIGHTS = "Lamp lights";
    public static final String HEATER = "Heater";
    public static final String POWER_STRIP_1 = "Power strip 1";
    public static final String POWER_STRIP_2 = "Power strip 2";
    public static final String POWER_SERVER = "Power server";
    public static final String KITCHEN_LIGHTS = "Kitchen lights";
    public static final String BEDROOM_LIGHTS = "Bedroom lights";
    public static final String SPEAKERS = "Speakers";
    public static final String ALL = "All";
    public static final char ON = '1';
    public static final char OFF = '0';
    public static final int PIN_7 = 7;
    public static final int PIN_6 = 6;
    public static final int PIN_5 = 5;
    public static final int PIN_1 = 1;

    @Value("${bot.token}")
    private String token;

    @Value("${bot.username}")
    private String username;



    char[] states = {OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF};

    boolean anyOn = false;

    boolean shareControl = false;

    @Autowired
    MqttPublisher mqttPublisher;

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();
            SendMessage response = new SendMessage();
            Long chatId = message.getChatId();
            response.setChatId(chatId);
            String text = message.getText();

            ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();

            List<KeyboardRow> keyboard = new ArrayList<>();

            KeyboardRow row = new KeyboardRow();
            row.add(LAMP_LIGHTS);
            row.add(HEATER);
            row.add(POWER_STRIP_1);
            keyboard.add(row);

            row = new KeyboardRow();
            row.add(POWER_STRIP_2);
            row.add(POWER_SERVER);
            row.add(KITCHEN_LIGHTS);
            keyboard.add(row);

            row = new KeyboardRow();
            row.add(BEDROOM_LIGHTS);
            row.add(SPEAKERS);
            row.add(ALL);
            keyboard.add(row);

            keyboardMarkup.setKeyboard(keyboard);
            response.setReplyMarkup(keyboardMarkup);

            String responseMessage = "";

            try {

                if(message.getFrom().getId() == 41852381 || shareControl) {

                    if (message.isCommand()) {

                        responseMessage = "By executing the command: ".concat(message.getText());

                        switch (message.getText()) {




                        }

                    } else {
                        switch (message.getText()) {
                            case LAMP_LIGHTS:
                                if (states[PIN_7] == ON) {

                                    mqttPublisher.toPost("tcp://localhost:2718","testApp","actuators","000,digitalWrite,13,false",0);
                                    states[PIN_7] = OFF;
                                    responseMessage = "The lamp was turned off correctly.";
                                } else {
                                    mqttPublisher.toPost("tcp://localhost:2718","testApp","actuators","000,digitalWrite,13,true",0);
                                    states[PIN_7] = ON;
                                    responseMessage = "The lamp was turned on correctly.";
                                }
                                break;
                            case HEATER:
                                if (states[PIN_6] == ON) {
                                    mqttPublisher.toPost("tcp://localhost:2718","testApp","actuators","001,digitalWrite,13,false",0);
                                    states[PIN_6] = OFF;
                                    responseMessage = "The heater was turned off correctly.";
                                } else {
                                    mqttPublisher.toPost("tcp://localhost:2718","testApp","actuators","001,digitalWrite,13,true",0);
                                    states[PIN_6] = ON;
                                    responseMessage = "The heater was turned on correctly.";
                                }
                                break;
                            case POWER_STRIP_1:
                                if (states[PIN_5] == ON) {
                                    mqttPublisher.toPost("tcp://localhost:2718","testApp","actuators","002,digitalWrite,13,false",0);
                                    states[PIN_5] = OFF;
                                    responseMessage = "The power strip #1 was turned off correctly.";
                                } else {
                                    mqttPublisher.toPost("tcp://localhost:2718","testApp","actuators","002,digitalWrite,13,true",0);
                                    states[PIN_5] = ON;
                                    responseMessage = "The strip #1 was turned on correctly.";
                                }
                                break;
                            case SPEAKERS:
                                if (states[PIN_1] == ON) {
                                    states[PIN_1] = OFF;
                                    responseMessage = "The speakers was turned off correctly.";
                                } else {
                                    states[PIN_1] = ON;
                                    responseMessage = "The speakers was turned on correctly.";
                                }
                                break;
                            case ALL:
                                if (anyOn) {
                                    char allOff[] = {OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF};
                                    states = allOff;
                                    anyOn = false;
                                    responseMessage = "Everything is off.";
                                } else {
                                    char allOn[] = {ON, ON, ON, ON, ON, ON, ON, ON};
                                    states = allOn;
                                    anyOn = true;
                                    responseMessage = "Everything is on and working for you.";
                                }
                                states[PIN_5] = ON;
                                break;

                            case "hola":
                            case "Hola":
                            case "Hola!":
                            case "Hello!":
                            case "Hello":
                            case "hi":
                            case "Hi":
                                    responseMessage = "Hola!, ".concat(message.getFrom().getFirstName());

                                break;
                            default:
                                responseMessage = "For the moment, \"" + message.getText() + "\" I can not interpret it as an order.";
                                break;
                        }
                    }


                }else {
                    responseMessage = "Unauthorized user to perform this operation.";

                }

                if(message.getFrom().getId() != 41852381) {
                    SendMessage responseToAdmin = new SendMessage();
                    responseToAdmin.setChatId(new Long(41852381));
                    responseToAdmin.setText(responseMessage.concat(" => "+message.getFrom().getFirstName()));
                    sendMessage(responseToAdmin);

                    SendMessage grupo = new SendMessage();
                    grupo.setChatId(new Long(-8322032));
                    grupo.setText(message.getFrom().getFirstName()+" is fooling around.");
                    sendMessage(grupo);
                }

                response.setText(responseMessage +" " + message.getFrom().getFirstName());
                sendMessage(response);
                logger.info("Sent message \"{}\" to {}, form {}", text, chatId, message.getFrom().getId());

            } catch (TelegramApiException e) {
                logger.error("Failed to send message \"{}\" to {} due to error: {}", text, chatId, e.getMessage());
            }
        }
    }

}
