package com.creando.iot.smartcontrol.mqtt.broker;

import io.moquette.spi.security.IAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;


public class MqttAuthenticatorConfig implements IAuthenticator {

    private static final Logger LOG = LoggerFactory.getLogger(MqttAuthenticatorConfig.class);

    private boolean securityEnabled = false;


    @Autowired
    IAuthenticator authenticator;


    @Override
    public boolean checkValid(String s, String s1, byte[] bytes) {
        if(securityEnabled) {
            try {

                return authenticator.checkValid(s,s1, bytes);
            } catch(BeansException e) {
                LOG.info("Could not load a IAuthenticator bean, disabling authenticator security");
                securityEnabled = false;
            }
        }

        //default is always valid
        return true;
    }
}