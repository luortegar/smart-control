package com.creando.iot.smartcontrol.mqtt.broker;

import io.moquette.spi.impl.subscriptions.Topic;
import io.moquette.spi.security.IAuthorizator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class MqttAuthorizatorConfig implements IAuthorizator {
    private static final Logger LOG = LoggerFactory.getLogger(MqttAuthorizatorConfig.class);

    private boolean securityEnabled = false;

    @Autowired
    IAuthorizator iAuthorizator;


    @Override
    public boolean canWrite(Topic topic, String s, String s1) {
        return true;
    }

    @Override
    public boolean canRead(Topic topic, String s, String s1) {
        return true;
    }
}