package com.creando.iot.smartcontrol.mqtt.broker;

import io.moquette.server.Server;
import io.moquette.server.config.MemoryConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Properties;


@Component
public class MqttBroker {
    private static final Logger LOG = LoggerFactory.getLogger(MqttBroker.class);

    @Value("${mqtt.server.port}")
    private int serverPort;

    @Value("${mqtt.host}")
    private String host;

    @Value("${mqtt.web.socket.port}")
    private int webSocketPort;

    private Server server;
    private MemoryConfig config;

    @PostConstruct
    public void start() throws IOException {
        LOG.info("Starting MQTT on host: {} and port: {} with websocket port: {}", host, serverPort, webSocketPort);

        config = new MemoryConfig(new Properties());
        config.setProperty("port", Integer.toString(serverPort));
        config.setProperty("websocket_port", Integer.toString(webSocketPort));
        config.setProperty("host", host);
        //config.setProperty("authenticator_class", MqttAuthenticatorConfig.class.getName());
       // config.setProperty("authorizator_class", MqttAuthorizatorConfig.class.getName());

        server = new Server();
        server.startServer(config);
        LOG.info("Moquette started successfully");
    }

    @PreDestroy
    public void stop() {
        server.stopServer();
    }
}
