package com.creando.iot.smartcontrol.service;


import gnu.io.NRSerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.util.TooManyListenersException;

//@Component
public class ArduinoConnectionService implements SerialPortEventListener {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private BufferedReader input;

    @Value("${arduinoPortName}")
    private String portName;

    @Value("${arduinoBaudRate}")
    private int baudRate;

    private NRSerialPort serial;

    @PostConstruct
    public void connect() throws TooManyListenersException {
        log.info("ArduinoConnection PostConstruct callback: connecting to Arduino...");

        serial = new NRSerialPort(portName, baudRate);
        serial.connect();

        if (serial.isConnected()) {
            log.info("Arduino connection opened!");
        }

        serial.addEventListener(this);
        serial.notifyOnDataAvailable(true);
        input = new BufferedReader(new InputStreamReader(serial.getInputStream()));
    }

    @PreDestroy
    public void disconnect() {

        log.info("ArduinoConnection PreDestroy callback: disconnecting from Arduino...");

        if (serial != null && serial.isConnected()) {
            serial.disconnect();

            if (!serial.isConnected()) {
                log.info("Arduino connection closed!");
            }
        }
    }

    public boolean write(String mensaje) {
        try {
            DataOutputStream stream = new DataOutputStream(serial.getOutputStream());
            stream.write(mensaje.getBytes());
            return true;
        } catch (Exception ex) {
            log.error("Error while sending control message: ", ex);
            return false;
        }
    }

    @Override
    public synchronized void serialEvent(SerialPortEvent serialPortEvent) {

        log.info("Leyendo");

        if (serialPortEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            log.info("Datos disponibles");
            try {
                String tarjetaId = null;
                if (input.ready()) {
                    tarjetaId = input.readLine();
                    System.out.println(tarjetaId);
                }

            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
    }


}
