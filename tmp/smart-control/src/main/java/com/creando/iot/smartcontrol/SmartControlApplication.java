package com.creando.iot.smartcontrol;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;


@SpringBootApplication
@EnableResourceServer
public class SmartControlApplication {


	public static void main(String[] args) {
		SpringApplication.run(SmartControlApplication.class, args);
	}
}
