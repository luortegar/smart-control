package com.creando.iot.smartcontrol.mqtt.broker;

import io.moquette.server.Server;
import io.moquette.server.config.MemoryConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Renze de Vries
 */
@Component
public class MQTTBroker {
    private static final Logger LOG = LoggerFactory.getLogger(MQTTBroker.class);

    @Value("${mqtt.serverport:1883}")
    private int serverPort=1885;

    @Value("${mqtt.host:0.0.0.0}")
    private String host="0.0.0.0";

    @Value("${mqtt.websocket_port:8083}")
    private int websocketPort=8086;

    private Server server;
    private MemoryConfig config;

    @PostConstruct
    public void start() throws IOException {
        LOG.info("Starting MQTT on host: {} and port: {} with websocket port: {}", host, serverPort, websocketPort);

        config = new MemoryConfig(new Properties());
        config.setProperty("port", Integer.toString(serverPort));
        config.setProperty("websocket_port", Integer.toString(websocketPort));
        config.setProperty("host", host);
   //     config.setProperty("authenticator_class", SpringAuthenticationWrapper.class.getName());
     //   config.setProperty("authorizator_class", SpringAuthorizationWrapper.class.getName());

        server = new Server();
        server.startServer(config);
        LOG.info("Moquette started successfully");
    }

    @PreDestroy
    public void stop() {
        server.stopServer();
    }
}
